/*
 * Nama : Muhammad Nabil Akbar Mustafa
 * NIM : 21/481679/PA/20981
 *
*/ 

#define pinled 13 // set led di pin 13

int toggle1; // deklarasi toggle1 sebagai integer

void setup()
{
  pinMode(pinled, OUTPUT); // pinmode untuk setting pinled sebagai output
  
  cli(); // stop interrupts
  TCCR1A = 0; // set seluruh register TCCR1A ke 0 
  TCCR1B = 0; // set seluruh register TCCR1B ke 0 
  TCNT1  = 0; // inisialisasi nilai counter ke 0
  // set membandingan match register untuk kenaikan 1hz 
  OCR1A = 15624;// = (16*10^6) / (1*1024) - 1 (harus <65536)
  TCCR1B |= (1 << WGM12); // nyalain CTC mode
  TCCR1B |= (1 << CS12) | (1 << CS10);  // Set CS12 dan CS10 bits untuk 1024 prescaler
  TIMSK1 |= (1 << OCIE1A); // aktifkan timer compare interrupt
  sei(); // mengizinkan interrupt
  
  attachInterrupt(digitalPinToInterrupt(3), turn_led_off, FALLING); // membuat tombol interrupt pada saat falling menuju fungsi turn_led_off 
}



ISR(TIMER1_COMPA_vect) // program timer interrupt agar led nge-blink 
{
  if (toggle1==0){
    digitalWrite (13,LOW);
    toggle1 =1;
  }
  else if (toggle1==1){
    digitalWrite (13,HIGH);
    toggle1 =0;
  }
}

void loop()
{
 // tidak mengerjakan apa-apa
}

void turn_led_off() // program hardware interrupt agar led mati selamanya 
{
  if (toggle1==0){ // pada saat led mati
    digitalWrite (13,LOW); //eksekusi agar program tidak nge-blink 
    toggle1 =5;
  }
  else if (toggle1==1){ // pada saat led nyala
    digitalWrite (13,LOW); //eksekusi agar program tidak nge-blink
    toggle1 =5;
  }
}
